﻿CREATE TABLE [dbo].[VideoGame]
(
	[Id] INT IDENTITY(1,1) NOT NULL, 
	[Name] NVarChar(20) NOT NULL,

	[Year] INT NULL, 
    [Genre] NVARCHAR(20) NULL, 
    [Rate] INT NOT NULL, 
    CONSTRAINT [PK_VideoGame] PRIMARY KEY CLUSTERED ([Id] ASC ),
)
