﻿IF NOT EXISTS (SELECT * FROM [dbo].[VideoGame])
BEGIN
	INSERT INTO [dbo].[VideoGame] ([Name],[Year],[Genre],[Rate])
     VALUES
           ('Pokémon Go', 2016, 'Mobile', 5),
		   ('Borderlands 2', 2012, 'War', 10),
		   ('Warcraft II', 1995, 'RTS', 7),
		   ('Dishonored', 2012, '', 4),
		   ('Final Fantasy VII', 1997, '', 8)
END