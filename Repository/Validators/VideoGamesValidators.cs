﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository
{
    internal static class VideoGamesValidators
    {
        private static class ErrorMessage
        {
            public const string IdShouldBePositive = "The Id should be a positive number greater than 0.";
            public const string NameIsRequired = "The Name is a required field.";
            public const string NameIsTooLong = "The Name is too long. Enter less than 20 characters.";
            public const string DuplicateName = "A game with this name already exists. Enter a different name.";
            public const string GenreIsTooLong = "The Genre is too long. Enter less than 20 characters.";
            public const string TooOld = "This game is too old (< 1980).";
            public const string TooNew = "This game is too new (Year in the future?).";
            public const string RateShouldBePositive = "The Rate is should be positive.";
            public const string RateShouldBeLessThan10 = "The Rate is should be less than 10.";
        }

        internal static void ValidateAdd(VideoGame dto, VideoGamesDbContext context)
        {
            if (dto.Id != 0)
                throw (new ArgumentException(ErrorMessage.IdShouldBePositive));

            ValidateName(dto.Id, dto.Name, context);
            ValidateGenre(dto.Genre);
            ValidateYear(dto.Year);
            ValidateRate(dto.Rate);
        }


        private static void ValidateName(int id, string name, VideoGamesDbContext context)
        {
            if (string.IsNullOrEmpty(name))
                throw (new ArgumentException(ErrorMessage.NameIsRequired));

            if (name.Length > 20)
                throw (new ArgumentException(ErrorMessage.NameIsTooLong));

            if(context.VideoGame.Any(x => x.Name == name && x.Id != id))
                throw (new ArgumentException(ErrorMessage.DuplicateName));
        }

        private static void ValidateGenre(string genre)
        {
            if (genre?.Length > 20)
                throw (new ArgumentException(ErrorMessage.GenreIsTooLong));
        }

        private static void ValidateYear(int? year)
        {
            if (year < 1980)
                throw (new ArgumentException(ErrorMessage.TooOld));

            if (year > DateTime.Now.Year)
                throw (new ArgumentException(ErrorMessage.TooNew));
        }

        private static void ValidateRate(int? rate)
        {
            if (rate < 0)
                throw (new ArgumentException(ErrorMessage.RateShouldBePositive));
            if (rate > 10)
                throw (new ArgumentException(ErrorMessage.RateShouldBeLessThan10));
        }

        internal static void ValidateGet(int id)
        {
            if(id <= 0)
                throw (new ArgumentException(ErrorMessage.IdShouldBePositive));
        }

        internal static void ValidateGetList()
        {
            //always valid
        }

        internal static void ValidateUpdate(VideoGame dto, VideoGamesDbContext contex)
        {
            if (dto.Id == 0)
                throw (new ArgumentException(ErrorMessage.IdShouldBePositive));

            ValidateName(dto.Id, dto.Name, contex);
            ValidateGenre(dto.Genre);
            ValidateYear(dto.Year);
            ValidateRate(dto.Rate);
        }

        internal static void ValidateDelete(int id)
        {
            if (id == 0)
                throw (new ArgumentException(ErrorMessage.IdShouldBePositive));
        }
    }
}
