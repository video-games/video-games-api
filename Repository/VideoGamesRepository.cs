﻿using Model;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Repository
{
    public interface IVideoGamesRepository
    {
        DTO.VideoGame AddVideoGame(DTO.VideoGame dto);
        DTO.VideoGame UpdateVideoGame(DTO.VideoGame dto);
        void DeleteVideoGame(int id);
        List<DTO.VideoGame> GetVideoGameList();
        DTO.VideoGame GetVideoGame(int id);
    }

    public class VideoGamesRepository : IVideoGamesRepository
    {
        private VideoGamesDbContext context;
        public VideoGamesRepository(VideoGamesDbContext context)
        {
            this.context = context;
        }

        public DTO.VideoGame AddVideoGame(DTO.VideoGame dtoToAdd)
        {
            VideoGamesAuthorizers.AuthorizeAdd();
            VideoGamesValidators.ValidateAdd(dtoToAdd, context);

            var model = new Model.VideoGame();
            VideoGameMapper.ToModel(dtoToAdd, model);
            context.VideoGame.Add(model);
            context.SaveChanges();

            var newDto = VideoGameMapper.ToDto(model);
            return newDto;
        }

        public void DeleteVideoGame(int id)
        {
            VideoGamesAuthorizers.AuthorizeDelete();
            VideoGamesValidators.ValidateDelete(id);

            var oldModel = context.VideoGame.FirstOrDefault(x => x.Id == id);

            if (oldModel == null)
            {
                //item was deleted already; do nothing
            }
            else
            {
                context.VideoGame.Remove(oldModel);
                context.SaveChanges();
            }
        }

        public DTO.VideoGame GetVideoGame(int id)
        {
            VideoGamesAuthorizers.AuthorizeGet();
            VideoGamesValidators.ValidateGet(id);

            var model = context.VideoGame.Find(id);
            if (model == null)
                throw new ArgumentException($"Cannot find Video Game with Id = {id}");

            var dto = VideoGameMapper.ToDto(model);
            return dto;
        }

        public List<DTO.VideoGame> GetVideoGameList()
        {
            VideoGamesAuthorizers.AuthorizeGet();
            VideoGamesValidators.ValidateGetList();

            var models = context.VideoGame.OrderBy(x => x.Name).ToList();
            var dto = models.Select(model => VideoGameMapper.ToDto(model)).ToList();
            return dto;
        }

        public DTO.VideoGame UpdateVideoGame(DTO.VideoGame dtoToUpdate)
        {
            VideoGamesAuthorizers.AuthorizeUpdate();
            VideoGamesValidators.ValidateUpdate(dtoToUpdate, context);

            var oldModel = context.VideoGame.FirstOrDefault(x => x.Id == dtoToUpdate.Id);

            if (oldModel == null)
            {
                //if the item was deleted (in a different browser) and it is updated now ... add it back as a new item
                dtoToUpdate.Id = 0;
                return AddVideoGame(dtoToUpdate);
            }
            else
            {
                VideoGameMapper.ToModel(dtoToUpdate, oldModel);
                context.VideoGame.Update(oldModel);
                context.SaveChanges();

                var dto = VideoGameMapper.ToDto(oldModel);
                return dto;
            }
        }
    }
}
