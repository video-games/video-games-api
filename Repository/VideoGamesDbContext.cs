﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Model;

namespace Repository
{
    public class VideoGamesDbContext : DbContext
    {
        private DbContextOptions<VideoGamesDbContext> _options;
        private string _connectionString;
        public VideoGamesDbContext(string connectionString)
        {
            this._connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (!string.IsNullOrEmpty(this._connectionString))
                options.UseSqlServer(this._connectionString);
            else
                base.OnConfiguring(options);
        }

        public VideoGamesDbContext(DbContextOptions<VideoGamesDbContext> options) : base(options)
        {
            this._options = options;
        }

        public virtual DbSet<VideoGame> VideoGame { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dbo");
            modelBuilder.Entity<VideoGame>().ToTable("VideoGame");
        }
    }
}
