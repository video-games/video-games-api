﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{
    public static class VideoGameMapper
    {
        public static DTO.VideoGame ToDto(Model.VideoGame model)
        {
            var dto = new DTO.VideoGame
            {
                Id = model.Id,
                Name = model.Name,
                Genre = model.Genre,
                Year = model.Year,
                Rate = model.Rate
            };

            return dto;
        }

        public static void ToModel(DTO.VideoGame dto, Model.VideoGame model)
        {
            model.Id = dto.Id;
            model.Name = dto.Name;
            model.Genre = dto.Genre;
            model.Year = dto.Year;
            model.Rate = dto.Rate;
        }
    }
}
