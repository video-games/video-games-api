﻿using DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{
    internal static class VideoGamesAuthorizers
    {
        internal static void AuthorizeAdd()
        {
            //do nothing -- all calls are authorized
        }

        internal static void AuthorizeGet()
        {
            //do nothing -- all calls are authorized
        }

        internal static void AuthorizeUpdate()
        {
            //do nothing -- all calls are authorized
        }

        internal static void AuthorizeDelete()
        {
            //do nothing -- all calls are authorized
        }
    }
}
