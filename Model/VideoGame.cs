﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class VideoGame
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(10)]
        public string Name { get; set; }

        public int? Year { get; set; }
        [MaxLength(10)]
        public string Genre { get; set; }

        public int Rate { get; set; }
    }
}
