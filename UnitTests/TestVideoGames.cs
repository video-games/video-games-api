﻿using Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTests
{
    class TestVideoGames: TestBase
    {
        private VideoGamesRepository getVideoGamesRepository()
        {
            return new VideoGamesRepository(this.GetNewContext());
        }

        [Fact]
        public void TestCreateVesselFromSvd()
        {
            var db = this.GetNewContext();
            var sql = "Select svd.* From StaticAndVoyageData svd left join vessel.Vessel v on svd.mmsi = v.mmsi Where svd.MMSI > 10000 and v.mmsi is null";
            var svd = db.StaticAndVoyageData.FromSqlRaw(sql).FirstOrDefault();
            if (svd == null)
                svd = db.StaticAndVoyageData.OrderByDescending(p => p.MMSI).FirstOrDefault();

            Assert.NotNull(svd);
            Assert.True(svd.MMSI > 0);

            int vslId = 0;
            var rep = this.getVesselRepository();
            var vsl = rep.GetOrCreateVesselFromStaticAndVoyageData(svd.MMSI, 2);
            Assert.NotNull(vsl);

            vslId = vsl.Id;
            Assert.True(vslId > 0);
            Assert.True(svd.MMSI == vsl.MMSI);
            Assert.Equal(svd.Name, vsl.Name);
            Assert.Equal(svd.ShipTypeName ?? "", vsl.IhsVesselType);
            Assert.Equal(svd.CallSign ?? "", vsl.CallSign);

            vsl = db.Vessel.Find(vslId);
            Assert.NotNull(vsl);

            db.Vessel.Remove(vsl);
            db.SaveChanges2();
            vsl = db.Vessel.Find(vslId);
            Assert.Null(vsl);
        }
    }
}
