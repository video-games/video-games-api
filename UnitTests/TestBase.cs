﻿using Microsoft.EntityFrameworkCore;
using Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTests
{
    public abstract class TestBase : IDisposable
    {
        internal TestBase()
        {
        }

        public virtual void Dispose()
        {
        }

        protected VideoGamesDbContext GetNewContext()
        {
            var cnnStr = "Data Source=.\\SQLEXPRESS;Initial Catalog=ezport-dover;Integrated Security=True";
            var builder = new DbContextOptionsBuilder<VideoGamesDbContext>();
            builder.UseSqlServer(cnnStr);
            return new VideoGamesDbContext(builder.Options);
        }
    }
}
