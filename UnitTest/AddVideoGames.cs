﻿using Repository;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace UnitTests
{
    public class AddVideoGames: TestBase
    {
        [Fact]
        public void AddVideoGame_Works()
        {
            var controller = GetController();
            var videoGame = NewVideoGame();
            var result = controller.AddVideoGame(videoGame);
            var status = GetStatus(result);
            Assert.Equal(200, status);
        }

        [Fact]
        public void AddVideoGame_Saves()
        {
            var controller = GetController();
            var videoGame = NewVideoGame();
            var result = controller.AddVideoGame(videoGame);
            var data = GetData(result);

            var id = data.Value<int>("Id");
            result = controller.GetVideoGame(id);
            var status = GetStatus(result);
            Assert.Equal(200, status);

            data = GetData(result);
            var name = data.Value<string>("Name");
            Assert.Equal(videoGame.Name, name);
        }

        [Fact]
        public void AddVideoGame_DuplicateNameFails()
        {
            var controller = GetController();
            var videoGame = NewVideoGame();
            var result = controller.AddVideoGame(videoGame);
            result = controller.AddVideoGame(videoGame);

            var status = GetStatus(result);
            Assert.Equal(400, status);
        }

        [Fact]
        public void AddVideoGame_TooOldFails()
        {
            var controller = GetController();
            var videoGame = NewVideoGame();
            videoGame.Year = 1970;
            var result = controller.AddVideoGame(videoGame);

            var status = GetStatus(result);
            Assert.Equal(400, status);
        }

        [Fact]
        public void AddVideoGame_OptionalFieldsNotRequired()
        {
            var controller = GetController();
            var videoGame = NewVideoGame();
            videoGame.Year = null;
            videoGame.Genre = null;
            var result = controller.AddVideoGame(videoGame);

            var status = GetStatus(result);
            Assert.Equal(200, status);
        }
    }
}
