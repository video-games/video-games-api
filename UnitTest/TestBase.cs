﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Repository;
using ServiceAPI.Controllers;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTests
{
    public abstract class TestBase : IDisposable
    {
        internal TestBase()
        {
        }

        public virtual void Dispose()
        {
        }

        protected VideoGamesDbContext GetInMemoryContext()
        {
            var options = new DbContextOptionsBuilder<VideoGamesDbContext>()
                    .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                    .Options;
            var databaseContext = new VideoGamesDbContext(options);
            databaseContext.Database.EnsureCreated();

            return databaseContext;
        }

        protected VideoGamesRepository GetRepository()
        {
            var context = GetInMemoryContext();
            var repository = new VideoGamesRepository(context);

            return repository;
        }

        protected VideoGamesController GetController()
        {
            var context = GetInMemoryContext();
            var repository = new VideoGamesRepository(context);
            var logger =  NullLogger<VideoGamesController>.Instance;
            var controller = new VideoGamesController(repository, logger);
            
            return controller;
        }

        protected int? GetStatus(JsonResult result)
        {
            var status = (int?)getJObject(result)["status"];
            return status;
        }

        protected dynamic GetData(JsonResult result)
        {
            var data = getJObject(result)["Data"];
            return data;
        }

        private JObject getJObject(JsonResult result)
        {
            var json = JsonConvert.SerializeObject(result.Value);
            var jResult = JObject.Parse(json);
            return jResult;
        }

        protected DTO.VideoGame NewVideoGame()
        {
            return new DTO.VideoGame
            {
                Id = 0,
                Genre = "Genre",
                Name = "Name",
                Rate = 5,
                Year = 2000
            };
        }
    }

}
