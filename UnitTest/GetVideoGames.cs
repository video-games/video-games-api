﻿using Newtonsoft.Json.Linq;
using Repository;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace UnitTests
{
    public class GetVideoGames: TestBase
    {
        [Fact]
        public void GetVideoGame_Works()
        {
            var controller = GetController();
            var videoGame = NewVideoGame();
            var result = controller.AddVideoGame(videoGame);
            
            var data = GetData(result);
            var id = data.Value<int>("Id");
            result = controller.GetVideoGame(id);

            var status = GetStatus(result);
            Assert.Equal(200, status);
        }

        [Fact]
        public void GetAllVideoGames_Works()
        {
            var controller = GetController();
            var videoGame = NewVideoGame();
            var result = controller.AddVideoGame(videoGame);

            result = controller.GetVideoGameList();
            var status = GetStatus(result);
            Assert.Equal(200, status);

            var data = (JArray)GetData(result);
            Assert.Equal(1, data.Count);
        }

        [Fact]
        public void GetVideoGame_InexistingIdFails()
        {
            var controller = GetController();
            var result = controller.GetVideoGame(1);

            var status = GetStatus(result);
            Assert.Equal(400, status);
        }
    }
}
