﻿using Repository;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace UnitTests
{
    public class UpdateVideoGames: TestBase
    {
        [Fact]
        public void UpdateVideoGame_Works()
        {
            var controller = GetController();
            var videoGame = NewVideoGame();
            var result = controller.AddVideoGame(videoGame);
            
            var data = GetData(result);
            var id = data.Value<int>("Id");
            result = controller.GetVideoGame(id);
            videoGame.Id = id;
            videoGame.Name = "Updated Name";

            result = controller.UpdateVideoGame(videoGame);
            var status = GetStatus(result);
            Assert.Equal(200, status);
        }

        [Fact]
        public void UpdateVideoGame_Saves()
        {
            var controller = GetController();
            var videoGame = NewVideoGame();
            var result = controller.AddVideoGame(videoGame);

            var data = GetData(result);
            var id = data.Value<int>("Id");
            result = controller.GetVideoGame(id);
            videoGame.Id = id;
            videoGame.Name = "Updated Name";

            result = controller.UpdateVideoGame(videoGame);
            var status = GetStatus(result);
            Assert.Equal(200, status);

            data = GetData(result);
            var name = data.Value<string>("Name");
            Assert.Equal(videoGame.Name, name);
        }

        [Fact]
        public void UpdateVideoGame_DuplicateNameFails()
        {
            var controller = GetController();
            var videoGame = NewVideoGame();
            var result = controller.AddVideoGame(videoGame);

            var data = GetData(result);
            var id = data.Value<int>("Id");
            videoGame.Id = id;
            videoGame.Name = "Updated Name";
            result = controller.UpdateVideoGame(videoGame);

            var status = GetStatus(result);
            Assert.Equal(200, status);

            //add another video game
            videoGame = NewVideoGame();
            result = controller.AddVideoGame(videoGame);

            data = GetData(result);
            id = data.Value<int>("Id");
            result = controller.GetVideoGame(id);
            videoGame.Id = id;
            videoGame.Name = "Updated Name";

            result = controller.UpdateVideoGame(videoGame);

            status = GetStatus(result);
            Assert.Equal(400, status);
        }
    }
}
