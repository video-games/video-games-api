﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceAPI
{
    [Produces("application/json")]
    public abstract partial class BaseController : Controller
    {
        protected ILogger logger;

        public BaseController(ILogger logger)
        {
            this.logger = logger;
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);

            try
            {
                var action = String.Format($"IP: {this.Request.HttpContext.Connection.RemoteIpAddress}; Action: {context.ActionDescriptor.DisplayName}");
                logger.LogInformation($"{action}");
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, "OnActionExcuted", context.ActionDescriptor.DisplayName);
            }
        }

        protected IActionResult SucceedResult(dynamic data = null)
        {
            return Ok(data);
        }

        protected IActionResult FailedResult(Exception ex = null, string message = "", dynamic data = null)
        {
            if (ex != null)
                this.logger.LogError(ex, message);

            if (ex is ArgumentException)
                return BadRequest(ex.Message);

            return Problem(statusCode: Microsoft.AspNetCore.Http.StatusCodes.Status500InternalServerError, detail: ex.Message);
        }
    }
}
