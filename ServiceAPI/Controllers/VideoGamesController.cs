﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ServiceAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VideoGamesController : BaseController
    {
        private IVideoGamesRepository repository;

        public VideoGamesController(IVideoGamesRepository repository, ILogger<VideoGamesController> logger) : base(logger)
        {
            this.repository = repository;
        }

        [HttpGet]
        public IActionResult GetVideoGameList()
        {
            try
            {
                var result = repository.GetVideoGameList();
                return this.SucceedResult(result);
            }
            catch (Exception ex)
            {
                return this.FailedResult(ex);
            }
        }

        [HttpGet("{id:int}")]
        public IActionResult GetVideoGame(int id)
        {
            try
            {
                var result = repository.GetVideoGame(id);
                return this.SucceedResult(result);
            }
            catch (Exception ex)
            {
                return this.FailedResult(ex);
            }
        }

        [HttpPut("add")]
        public IActionResult AddVideoGame([FromBody] DTO.VideoGame videoGame)
        {
            try
            {
                var result = repository.AddVideoGame(videoGame);
                return this.SucceedResult(result);
            }
            catch (Exception ex)
            {
                return this.FailedResult(ex);
            }
        }

        [HttpPut("update")]
        public IActionResult UpdateVideoGame([FromBody] DTO.VideoGame videoGame)
        {
            try
            {
                var result = repository.UpdateVideoGame(videoGame);
                return this.SucceedResult(result);
            }
            catch (Exception ex)
            {
                return this.FailedResult(ex);
            }
        }

        [HttpDelete("{id:int}/delete")]
        public IActionResult DeleteVideoGame(int id)
        {
            try
            {
                repository.DeleteVideoGame(id);
                return this.SucceedResult();
            }
            catch (Exception ex)
            {
                return this.FailedResult(ex);
            }
        }
    }
}
